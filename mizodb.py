from ZODB import FileStorage, DB
import transaction
from datetime import date
from sqlalchemy import Column, DateTime, String, Integer
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import relationship
from sqlalchemy.schema import ForeignKey
from sqlalchemy import create_engine
from sqlalchemy.orm import Session

Base = declarative_base()
metadata = Base.metadata


class MiZODB(object):
    def __init__(self, archivo):
        self.storage = FileStorage.FileStorage(archivo)
        self.db = DB(self.storage)
        self.conexion = self.db.open()
        self.raiz = self.conexion.root()

    def close(self):
        self.conexion.close()
        self.db.close()
        self.storage.close()


class Socio(Base):
    __tablename__ = 'SOCIO'

    COD_SOC = Column(Integer, primary_key=True)
    NOMBRE = Column(String(20))
    APELLIDOS = Column(String(20))
    DIRECCION = Column(String(20))
    TELEFONO = Column(String(10))
    POBLACION = Column(String(30), ForeignKey('POBLACION.NOMBRE'))
    CP = Column(String(5))
    PROVINCIA = Column(String(20))
    PAIS = Column(String(10))
    EDAD = Column(Integer)
    FECHAALTA = Column(DateTime, default=date.today())
    CUOTA = Column(Integer)

    poblacionfk = relationship("Poblacion", foreign_keys=[POBLACION], viewonly=True)
    children = relationship("Socio_Pelicula")


class Poblacion(Base):
    __tablename__ = 'POBLACION'

    NOMBRE = Column(String(20), primary_key=True)

    children = relationship("Socio")


class Pelicula(Base):
    __tablename__ = 'PELICULA'

    COD_PEL = Column(Integer, primary_key=True)
    TITULO = Column(String(20))
    DURACION = Column(Integer)
    DIRECTOR = Column(String(20))

    children = relationship("Socio_Pelicula", uselist=False)


class Socio_Pelicula(Base):
    __tablename__ = 'TIENE'

    COD_SOC = Column(Integer, ForeignKey('SOCIO.COD_SOC'), primary_key=True)
    COD_PEL = Column(Integer, ForeignKey('PELICULA.COD_PEL'), primary_key=True)

    codSocFK = relationship("Socio", foreign_keys=[COD_SOC], viewonly=True)
    codPelFK = relationship("Pelicula", foreign_keys=[COD_PEL], viewonly=True)


def migrarDatos():
    connect_string = 'mysql+pymysql://{}:{}@{}:{}/{}?charset=utf8'.format("CPMWMli5c3", "4LcKkgc4qo", "remotemysql.com",
                                                                          "3306", "CPMWMli5c3")

    engine = create_engine(connect_string)
    Base = automap_base()
    Base.prepare(engine, reflect=True)
    session = Session(engine)

    print(":: MIGRANDO DATOS ::")
    db = MiZODB('bd_videoclub.fs')
    dbroot = db.raiz
    socios = []
    peliculas = []
    tienen = []
    for socio in session.query(Socio).all():
        print("Codigo de socio: ", socio.COD_SOC,
              " Nombre: ", socio.NOMBRE,
              " Apellidos: ", socio.APELLIDOS,
              " Direccion: ", socio.DIRECCION);

        sociozodb = Socio()

        sociozodb.COD_SOC = socio.COD_SOC
        sociozodb.NOMBRE = socio.NOMBRE
        sociozodb.APELLIDOS = socio.APELLIDOS
        sociozodb.DIRECCION = socio.DIRECCION
        sociozodb.TELEFONO = socio.TELEFONO
        sociozodb.POBLACION = socio.POBLACION
        sociozodb.CP = socio.CP
        sociozodb.PROVINCIA = socio.PROVINCIA
        sociozodb.PAIS = socio.PAIS
        sociozodb.EDAD = socio.EDAD
        sociozodb.CUOTA = socio.CUOTA

        socios.append(sociozodb)
    for pelicula in session.query(Pelicula).all():
        print("Codigo de pelicula: ", pelicula.COD_PEL,
              " Titulo: ", pelicula.TITULO,
              " Duracion: ", pelicula.DURACION,
              " Director: ", pelicula.DIRECTOR)

        peliculazodb = Pelicula()

        peliculazodb.COD_PEL = pelicula.COD_PEL
        peliculazodb.TITULO = pelicula.TITULO
        peliculazodb.DURACION = pelicula.DURACION
        peliculazodb.DIRECTOR = pelicula.DIRECTOR
        peliculas.append(peliculazodb)
    dbroot['PELICULA'] = peliculas
    for tiene in session.query(Socio_Pelicula).all():
        print("Codigo de pelicula: ", tiene.COD_PEL,
              "Codigo de socio: ", tiene.COD_SOC)

        tienezodb = Socio_Pelicula()

        tienezodb.COD_PEL = tiene.COD_PEL
        tienezodb.COD_SOC = tiene.COD_SOC
        tienen.append(tienezodb)
    dbroot['TIENE'] = tienen

    transaction.commit()
    # for val in dbroot['socios']:
    #    print(val.COD_SOC)
    print('')
    db.close()


def printUsers():
    print(":: LISTAR DATOS ::")
    db = MiZODB('bd_videoclub.fs')
    dbroot = db.raiz
    socios = dbroot['SOCIO']
    peliculas = dbroot['PELICULA']
    tienen = dbroot['TIENE']
    for socio in socios:
        print("Codigo de socio: ", socio.COD_SOC,
              " Nombre: ", socio.NOMBRE,
              " Apellidos: ", socio.APELLIDOS,
              " Direccion: ", socio.DIRECCION,
              " Cuota: ", socio.CUOTA)
    db.close()


def addPartner():
    print(":: DONAR D'ALTA UN USUARI ::")
    socio = Socio()
    socio.COD_SOC = idPartner()
    db = MiZODB('bd_videoclub.fs')
    dbroot = db.raiz
    socios = dbroot['SOCIO']
    socio.NOMBRE = input("Nom: ")
    socio.APELLIDOS = input("Cognom: ")
    socio.DIRECCION = input("Direccio: ")
    socio.TELEFONO = input("Telefon: ")
    socio.POBLACION = input("Poblacion: ")
    socio.CP = input("CP: ")
    socio.PROVINCIA = input("Provincia: ")
    socio.PAIS = input("Pais: ")
    socio.EDAD = int(input("Edat: "))
    socio.CUOTA = int(input("Quota: "))
    socios.append(socio)
    dbroot['SOCIO'] = socios
    transaction.commit()
    db.close()
    print("record inserted")
    printUsers()


def idPartner():
    print(":: DONAR D'ID A UN USUARI ::")
    db = MiZODB('bd_videoclub.fs')
    dbroot = db.raiz
    socios = dbroot['SOCIO']
    socio = dbroot['SOCIO'][len(socios) - 1]
    db.close()
    print(socio.COD_SOC + 1)
    return socio.COD_SOC + 1


def deletePartner():
    printUsers()
    print(":: ELIMINAR SOCI ::")
    codSoc = int(input("Quin usuari es vol eliminar? (Introdueix COD_SOC)"))
    db = MiZODB('bd_videoclub.fs')
    dbroot = db.raiz
    socios = dbroot['SOCIO']
    for socio in socios:
        print(socio.COD_SOC)
        if socio.COD_SOC == codSoc:
            print("Codigo de socio: " + str(
                socio.COD_SOC) + " Nombre: " + socio.NOMBRE + " Apellidos: " + socio.APELLIDOS + " Direccion: " + socio.DIRECCION);
            conf = input("Estas seguro que el vols eliminar?(S/N)")
            if conf.lower() == "s":
                socios.remove(socio)
                dbroot['SOCIO'] = socios
                transaction.commit()
                print("record deletied")
    db.close()


def editPartner():
    printUsers()
    print(":: CANVI DE QUOTA DE SOCI ::\n")
    codSoc = int(input("Quin usuari es vol modificar? (Introdueix COD_SOC)"))
    db = MiZODB('bd_videoclub.fs')
    dbroot = db.raiz
    socios = dbroot['SOCIO']
    for x in range(0, len(socios)):
        if socios[x].COD_SOC == codSoc:
            print(
                "Codigo de socio: " + str(socios[x].COD_SOC) + " Nombre: " + socios[x].NOMBRE + " Apellidos: " + socios[
                    x].APELLIDOS +
                " Direccion: " + socios[x].DIRECCION)
            conf = input("Es vol canviar la cuota?(S/N))")
            if conf.lower() == "s":
                socios[x].CUOTA = int(input("Introdueix la quantitat de la cuota: "))
                dbroot['SOCIO'] = socios
                transaction.commit()
                print("record modyfied")
    db.close()


def menu():
    print("----------- MAIN MENU -----------")
    print("1. Donar d'alta un usuari")
    print("2. Modificar quota de soci")
    print("3. Eliminar un soci")
    print("4. Llistar un soci")
    print("5. Migrar datos")
    print("6. Exit")
    print("-----------------------------------")


while True:
    menu()
    opcion = int(input("Selecciona una opcio: "))
    if opcion == 1:
        addPartner()
        print()
    elif opcion == 2:
        editPartner()
        print()
    elif opcion == 3:
        deletePartner()
        print()
    elif opcion == 4:
        printUsers()
        print()
    elif opcion == 5:
        migrarDatos()
        printUsers()
        print()
    elif opcion == 6:
        break
    else:
        print("Introdueix un numero entre 1 i 5")
